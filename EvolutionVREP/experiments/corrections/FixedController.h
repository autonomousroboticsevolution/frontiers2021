#ifndef FIXEDCONTROLLER_H
#define FIXEDCONTROLLER_H

#include "ARE/Control.h"
#include <multineat/NeuralNetwork.h>

namespace are {

class FixedController : public Control
{
public:
    FixedController() : Control(){}
    FixedController(const NEAT::NeuralNetwork &n) : nn(n){}
    FixedController(const FixedController& ctrl) : Control(ctrl), nn(ctrl.nn){}

    Control::Ptr clone() const override {
        return std::make_shared<FixedController>(*this);
    }

    std::vector<double> update(const std::vector<double> &sensorValues) override;


    NEAT::NeuralNetwork nn;

};

}

#endif //FIXEDCONTROLLER_H
