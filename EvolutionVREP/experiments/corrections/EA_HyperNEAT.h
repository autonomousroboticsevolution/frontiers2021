#ifndef CORRECTIONS_H
#define CORRECTIONS_H

#include "ARE/EA.h"
#include "CPPNGenome.h"
#include "CPPNIndividual.h"
#include "VoxelMorphology.h"

#include "eigen3/Eigen/Core"

#include "Random.h"
#include "misc/RandNum.h"

namespace are {

class Corrections : public EA
{
public:
    Corrections() : EA(){}
    Corrections(const settings::ParametersMapPtr& param) : EA(param){}
    ~Corrections() override {}

    void init() override;
    void epoch() override;

    /**
     * @brief Estimates the novelty of a specific individual
     */
    float novelty(const Individual::Ptr&);

    /**
     * @brief Gets the ns descriptor
     */
    Eigen::VectorXf morphDesc(const Individual::Ptr& ind);

    void getIndividuals() ;


private:
    std::unique_ptr<NEAT::Population> morph_population;

    /**
     * @brief Measure the distances of a single point with respect all the archive
     */
    std::vector<float> distances(const Eigen::VectorXf&);

    NEAT::Parameters params;
    int indCounter;
    std::vector<int> individuals;
};

}

#endif //EA_HYPERNEAT_H
