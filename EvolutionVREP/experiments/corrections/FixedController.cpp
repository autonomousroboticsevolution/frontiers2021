#include "FixedController.h"

using namespace are;

std::vector<double> FixedController::update(const std::vector<double> &sensorValues)
{
    std::vector<double> wheelsSpeed;
    // Calculate speed for motors linearly proportional to the average sensor readings.
    wheelsSpeed.push_back((sensorValues[1]*1.5708)/0.2);
    wheelsSpeed.push_back((sensorValues[0]*1.5708)/0.2);
    return wheelsSpeed;
}
