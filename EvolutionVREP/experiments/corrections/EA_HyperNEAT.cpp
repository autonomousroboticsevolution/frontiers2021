#include "EA_HyperNEAT.h"

using namespace are;

void Corrections::init()
{
    indCounter = 0;
    getIndividuals();
//    setNumOfGen(individuals.size());
    std::cout << "Number of individuals is: " << individuals.size() << std::endl;
    // Morphology
    //NEAT::Genome morph_genome(0, 4, 8, 5, false, NEAT::SIGNED_SIGMOID, NEAT::SIGNED_SIGMOID, 0, params, 4);
    NEAT::Genome morph_genome(0, 4, 10, 5, false, NEAT::SIGNED_SIGMOID, NEAT::SIGNED_SIGMOID, 0, params, 10);
    morph_population = std::make_unique<NEAT::Population>(morph_genome, params, true, 1.0, randomNum->getSeed());
    unsigned int pop_size = settings::getParameter<settings::Integer>(parameters,"#populationSize").value;
    for (int i = 0; i < pop_size; i++)
    {
        EmptyGenome::Ptr no_gen(new EmptyGenome);
        CPPNGenome::Ptr morphgenome(new CPPNGenome);
        CPPNIndividual::Ptr ind(new CPPNIndividual(morphgenome,no_gen));
        ind->set_parameters(parameters);
        ind->set_individual_id(individuals[indCounter]);
        population.push_back(ind);
        indCounter++;
    }
}

void Corrections::epoch(){
    population.clear();
    int pop_size = settings::getParameter<settings::Integer>(parameters,"#populationSize").value;
    for (int i = 0; i < pop_size ; i++)
    {
        EmptyGenome::Ptr no_gen(new EmptyGenome);
        CPPNGenome::Ptr morphgenome(new CPPNGenome);
        CPPNIndividual::Ptr ind(new CPPNIndividual(morphgenome,no_gen));
        ind->set_parameters(parameters);
        ind->set_individual_id(individuals[indCounter]);
        population.push_back(ind);
        indCounter++;
    }
}

float Corrections::novelty(const Individual::Ptr& ind){
    return 1.0;
}

Eigen::VectorXf Corrections::morphDesc(const Individual::Ptr& ind)
{
//    Morphology::Ptr morph = ind->get_morphology();
//    Eigen::VectorXf morphDesc = morph->getMorphDesc();
//    morph.reset();
//    return morphDesc;
}

std::vector<float> Corrections::distances(const Eigen::VectorXf& ind_md){
    std::vector<float> dist;
    return dist;
}

void Corrections::getIndividuals()
{
    individuals.clear();
    std::string line;
    std::string fileName = settings::getParameter<settings::String>(parameters,"#noRepTestsFile").value;
    std::string experimentName = settings::getParameter<settings::String>(parameters,"#loadExperiment").value;
    std::string repPath = settings::getParameter<settings::String>(parameters,"#expRep").value;
    std::ifstream testsFile(repPath + experimentName + fileName);

    std::vector<std::string> vectorStrings;
    if (testsFile.is_open())
    {
        while(getline(testsFile,line))
        {
            std::stringstream tempSS(line);
            while(getline(tempSS,line,',')){
                individuals.push_back(std::stoi(line));
                break;
            }
        }
        testsFile.close();
    }
    else
    {
        std::cerr << "Unable to open file" << std::endl;
        abort();
    }
}
