#ifndef CPPNINDIVIDUAL_H
#define CPPNINDIVIDUAL_H

#include "ARE/Individual.h"
#include "CPPNGenome.h"
#include "FixedController.h"
#include "VoxelMorphology.h"
#include "v_repLib.h"
#include "eigen_boost_serialization.hpp"

namespace are {

class CPPNIndividual : public Individual
{
public :
    CPPNIndividual() : Individual(){}
    CPPNIndividual(const Genome::Ptr& morph_gen,const Genome::Ptr& ctrl_gen);
//    CPPNIndividual(const CPPNIndividual& ind) :
//            Individual(ind), nn(ind.nn), morphDesc(ind.morphDesc), testRes(ind.testRes){}

    Individual::Ptr clone();

    void update(double delta_time);

    template<class archive>
    void serialize(archive &arch, const unsigned int v)
    {
        arch & morphGenome;
        arch & morphDesc;
//        arch & testRes;
//        arch & rawMat;
//        arch & protoPhenotype;
        //arch & nn;

        // arch & manValue
        // arch & noveltyValue; /// \todo EB: I don't think I need this because this computed at the end of the generation.
    }
    // Serialization
    std::string to_string();
    void from_string(const std::string &str);

    // Setters and getters
    NEAT::NeuralNetwork getGenome(){return nn;};
    Eigen::VectorXd getMorphDesc(){return morphDesc;};
    std::vector<bool> getManRes(){return testRes;};
    std::vector<std::vector<float>> getRawMat(){return rawMat;};

    void setGenome();
    void setMorphDesc();
    void setManRes();
    void setRawMat();

    NEAT::NeuralNetwork getCPPN();

protected:
    void createController() override;
    void createMorphology() override;

    NEAT::NeuralNetwork nn;
    Eigen::VectorXd morphDesc;
    std::vector<bool> testRes;
    std::vector<std::vector<float>> rawMat;
};

}//are

#endif //CPPNINDIVIDUAL_H
