#include "CPPNIndividual.h"

using namespace are;

CPPNIndividual::CPPNIndividual(const Genome::Ptr& morph_gen,const Genome::Ptr& ctrl_gen) :
    Individual(morph_gen,ctrl_gen)
{
//    createMorphology();
//    createController();
}

Individual::Ptr CPPNIndividual::clone()
{
    return std::make_shared<CPPNIndividual>(*this);
}

void CPPNIndividual::update(double delta_time)
{
    if(!morphology->stopSimulation){
//        std::vector<double> inputs = ns->update();
//        std::vector<double> outputs = control->update(inputs);
//        ns->updateActuators(outputs);
    }
    else{
        simStopSimulation();
    }
}

void CPPNIndividual::createMorphology()
{
    morphology.reset(new VoxelMorphology(parameters));
    NEAT::NeuralNetwork nn;
    nn = getCPPN();
    std::dynamic_pointer_cast<VoxelMorphology>(morphology)->setGenome(nn);
    morphology->createAtPosition(0,0,0.22);

    setGenome();
    setMorphDesc();
    setManRes();
    setRawMat();
}

void CPPNIndividual::createController()
{
    control.reset(new FixedController);
}

void CPPNIndividual::setGenome()
{
    nn = std::dynamic_pointer_cast<VoxelMorphology>(morphology)->getGenome();
}

void CPPNIndividual::setMorphDesc()
{
    morphDesc = std::dynamic_pointer_cast<VoxelMorphology>(morphology)->getMorphDesc();
}

void CPPNIndividual::setManRes()
{
    testRes = std::dynamic_pointer_cast<VoxelMorphology>(morphology)->getRobotManRes();
}

void CPPNIndividual::setRawMat()
{
    rawMat = std::dynamic_pointer_cast<VoxelMorphology>(morphology)->getRawMatrix();
}

std::string CPPNIndividual::to_string()
{
    std::stringstream sstream;
    boost::archive::text_oarchive oarch(sstream);
    oarch.register_type<CPPNIndividual>();
    oarch.register_type<CPPNGenome>();
//    oarch.register_type<EmptyGenome>();
    oarch << *this;
    return sstream.str();
}

void CPPNIndividual::from_string(const std::string &str){
    std::stringstream sstream;
    sstream << str;
    boost::archive::text_iarchive iarch(sstream);
    iarch.register_type<CPPNIndividual>();
    iarch.register_type<CPPNGenome>();
//    iarch.register_type<EmptyGenome>();
    iarch >> *this;
}

NEAT::NeuralNetwork CPPNIndividual::getCPPN()
{
    NEAT::NeuralNetwork nn;
    std::cout << "Loading genome " << individual_id << "!" << std::endl;
    std::string repPath = settings::getParameter<settings::String>(parameters,"#expRep").value;
    std::string loadExperiment = settings::getParameter<settings::String>(parameters,"#loadExperiment").value;
    std::stringstream filepath;
    filepath << repPath << loadExperiment << "/genome" <<individual_id;
    nn.Clear();
    nn.Load(filepath.str().c_str());
    return nn;
}
